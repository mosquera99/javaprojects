package DTO;

public class TiposConsolas {
	
	private int idipoConsola;
	private String nombreTipoConsola;
	private String[] tiposConsolas;
	private String[] accesoriosConsola;
	private int valorTipoConsola;
	
	public String[] getAccesoriosConsola() {
		return accesoriosConsola;
	}
	public void setAccesoriosConsola(String[] accesoriosConsola) {
		this.accesoriosConsola = accesoriosConsola;
	}
	public int getIdipoConsola() {
		return idipoConsola;
	}
	public void setIdipoConsola(int idipoConsola) {
		this.idipoConsola = idipoConsola;
	}
	public String getNombreTipoConsola() {
		return nombreTipoConsola;
	}
	public void setNombreTipoConsola(String nombreTipoConsola) {
		this.nombreTipoConsola = nombreTipoConsola;
	}
	public String[] getTiposConsolas() {
		return tiposConsolas;
	}
	public void setTiposConsolas(String[] tiposConsolas) {
		this.tiposConsolas = tiposConsolas;
	}
	public int getValorTipoConsola() {
		return valorTipoConsola;
	}
	public void setValorTipoConsola(int valorTipoConsola) {
		this.valorTipoConsola = valorTipoConsola;
	}
	
	
}
