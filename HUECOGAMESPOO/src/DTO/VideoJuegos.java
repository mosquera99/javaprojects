package DTO;

import java.util.ArrayList;
import java.util.Arrays;

public class VideoJuegos {

    private int idJuego;
    private String nombreJuego;
    private ArrayList<String> categoriaJuegos = new ArrayList<>(Arrays.asList("Acción","Aventura","Deportes","Infantil"));
    private ArrayList<String> juegosDisponiblesMay = new ArrayList<>(Arrays.asList("Fifa 19","God of war 3","Saint row 2","Chicos del barrio",
        "Call of duty blkps 3","Crahs Bandiko"));
    private ArrayList<String> juegosDisponiblesMen = new ArrayList<>(Arrays.asList("Fifa 19","Chicos del barrio","Crahs Bandiko"));
    private int edad;
    private int valorVideojuego;

    public VideoJuegos() {
    }

    public int getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(int idJuego) {
        this.idJuego = idJuego;
    }

    public String getNombreJuego() {
        return nombreJuego;
    }

    public void setNombreJuego(String nombreJuego) {
        this.nombreJuego = nombreJuego;
    }

    public ArrayList<String> getCategoriaJuegos() {
        return categoriaJuegos;
    }

    public void setCategoriaJuegos(ArrayList<String> categoriaJuegos) {
        this.categoriaJuegos = categoriaJuegos;
    }

    public ArrayList<String> getJuegosDisponiblesMay() {
        return juegosDisponiblesMay;
    }

    public void setJuegosDisponiblesMay(ArrayList<String> juegosDisponiblesMay) {
        this.juegosDisponiblesMay = juegosDisponiblesMay;
    }

    public ArrayList<String> getJuegosDisponiblesMen() {
        return juegosDisponiblesMen;
    }

    public void setJuegosDisponiblesMen(ArrayList<String> juegosDisponiblesMen) {
        this.juegosDisponiblesMen = juegosDisponiblesMen;
    }
    
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getValorVideojuego() {
        return valorVideojuego;
    }

    public void setValorVideojuego(int valorVideojuego) {
        this.valorVideojuego = valorVideojuego;
    }
}
