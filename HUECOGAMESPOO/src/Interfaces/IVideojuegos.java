package Interfaces;

import DTO.VideoJuegos;

public interface IVideojuegos {
        
        public void getCategoriasGames();
    
	public void getGamesDisponibles();
        
	public VideoJuegos pedirGame(VideoJuegos idJuego,VideoJuegos nombreJuego,
			VideoJuegos categoriaJuego,VideoJuegos isMayorEdad,VideoJuegos valorVideojuego);
}
