package Interfaces;

import DTO.TiposConsolas;

public interface ITipoConsolas {
	
	public void getConsolasDisponibles();

	public TiposConsolas pedirConola(TiposConsolas idipoConsola,TiposConsolas nombreTipoConsola,
			TiposConsolas tiposConsolas,TiposConsolas accesoriosConsola,TiposConsolas valorTipoConsola);
}
