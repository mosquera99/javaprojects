package manejojavabeans;

import beans.Persona;

public class manejoJavaBeans {

    public static void main(String[] args) {

        Persona persona = new Persona();
        Persona persona2 = new Persona();
        persona.setNombre("Juan");
        persona.setEdad(39);
        persona2.setNombre("Antonio");
        persona2.setEdad(45);

        System.out.println("Nombre : " + persona.getNombre());
        System.out.println("Edad : " + persona.getEdad() + "\n");
        System.out.println("Nombre : " + persona2.getNombre());
        System.out.println("Edad : " + persona2.getEdad());
    }
}
