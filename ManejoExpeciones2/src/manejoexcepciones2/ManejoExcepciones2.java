package manejoexcepciones2;
import Excepciones.*;
import datos.*;

public class ManejoExcepciones2 {

    public static void main(String[] args) throws AccesoDatosEx {

        AccesoDatos datos = new ImplMySql();
        datos.simularError(true);
        ejecutar(datos,"listar");

        System.out.println(" ");
        datos.simularError(true);
        ejecutar(datos,"registar");
    }

    public static void ejecutar(AccesoDatos datos, String accion) {
        if("listar".equals(accion)){
            try {
                datos.listar();
            }catch (LecturaDatosExc ex){
                System.out.println("Error de lectura de datos");
            }catch (AccesoDatosEx ae){
                System.out.println("Error expcion padre");
            }catch (Exception e){
                System.out.println("Error general");
            }
        }else if("registar".equals(accion)){
            try {
                datos.registar();
            }catch (AccesoDatosEx ae){
                System.out.println("Error expcion padre");
            }finally{
                System.out.println("Termina la aplicacion");
            }
        }else {
            System.out.println("No a ingresado una accion valida");
        }
    }
}
