package datos;

import  Excepciones.AccesoDatosEx;

public interface AccesoDatos {

    public abstract void listar() throws AccesoDatosEx;
    public abstract void registar() throws AccesoDatosEx;
    public abstract void simularError(boolean simular) throws AccesoDatosEx;
}
