package datos;

import Excepciones.*;

public class ImplMySql implements AccesoDatos {

    private boolean simularError;

    @Override
    public void listar() throws AccesoDatosEx {
        if(simularError){
            throw new LecturaDatosExc("Error leyendo los datos");
        }else{
            System.out.println("Consultar los datos de bd");
        }
    }

    @Override
    public void registar() throws AccesoDatosEx {
        if(simularError){
            throw new EscrituraDatosExc("Error insertando los datos");
        }else{
            System.out.println("Insertar los datos en la bd");
        }
    }

    public boolean isSimularError() {
        return simularError;
    }

    @Override
    public void simularError(boolean simular) throws AccesoDatosEx {
        this.simularError = simular;
    }
}
