package enums;

public enum Operaciones {

    SUMAR("SUMAR"),
    RESTAR("RESTAR"),
    MULTI("MULTI"),
    DIVIDIR("DIVIDIR");

    private final String operacion;

    Operaciones(String operacion) {
        this.operacion = operacion;
    }


}
