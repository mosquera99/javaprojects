package enums;

import calculadora.OperacionesDef;

import java.util.Scanner;

public class OperacionesImpl {

    public static void realizarOperaciones(Operaciones operaciones){
        Scanner scanner = new Scanner(System.in);
        switch (operaciones){
            case SUMAR:
                System.out.println("Estas en la operación sumar");
                System.out.println("Introduce el primer numero para la operacion");
                int a = scanner.nextInt();
                System.out.println("Introduce el segundo numero para la operacion");
                int b = scanner.nextInt();
                System.out.println("El resultado de la operación es: "+ OperacionesDef.sumar(a,b));
                System.out.println("\n");
                break;
            case RESTAR:
                System.out.println("Estas en la operación restar");
                System.out.println("Introduce el primer numero para la operacion");
                int c = scanner.nextInt();
                System.out.println("Introduce el segundo numero para la operacion");
                int d = scanner.nextInt();
                System.out.println("El resultado de la operación es: "+ OperacionesDef.restar(c,d));
                System.out.println("\n");
                break;
            case MULTI:
                System.out.println("Estas en la operación multi");
                System.out.println("Introduce el primer numero para la operacion");
                int e = scanner.nextInt();
                System.out.println("Introduce el segundo numero para la operacion");
                int f = scanner.nextInt();
                System.out.println("El resultado de la operación es: "+ OperacionesDef.multi(e,f));
                System.out.println("\n");
                break;
            case DIVIDIR:
                System.out.println("Estas en la operación dividir");
                System.out.println("Introduce el primer numero para la operacion");
                int g = scanner.nextInt();
                System.out.println("Introduce el segundo numero para la operacion");
                int h = scanner.nextInt();
                System.out.println("El resultado de la operación es: "+ OperacionesDef.divi(g,h));
                System.out.println("\n");
                break;
            default:
                System.out.println("No se a obtenido la operación a realizar");
        }
    }
}
