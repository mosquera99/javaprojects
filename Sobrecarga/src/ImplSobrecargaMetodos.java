public class ImplSobrecargaMetodos {

    public static void main(String[] args) {
        System.out.println("Resultado 1: \n"+SobrecargaMetodos.sumar(4,9));
        System.out.println("Resultado 2: \n"+SobrecargaMetodos.sumar(4,5.2));
        System.out.println("Resultado 3: \n"+SobrecargaMetodos.sumar(4.3,4.3));

    }
}
