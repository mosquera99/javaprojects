public class SobrecargaMetodos {

    public static int sumar(int a,int b){
        System.out.println("RESULTADO INT: ");
        return a+b;
    }

    public static double sumar(int a, double b){
        System.out.println("RESULTADO INT Y DOUBLE: ");
        return a+b;
    }

    public static double sumar(double a,double b){
        System.out.println("RESULTADO DOUBLE: ");
        return a+b;
    }
}
