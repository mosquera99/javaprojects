package Ejemploconversionobject;

public class Main {

    public static void main(String[] args) {
        Empleado empleado;

        //Esto es upcating
        empleado = new Escritor("Roy",9000,TipoEscritura.CLASICO);
        imprimirDetalles(empleado);

        empleado = new Gerente("Luis",20000,"Gerencia");
        imprimirDetalles(empleado);
    }

    private static void imprimirDetalles(Empleado empleado){
        String resultado = null;

        System.out.println("\n Detalles: "+empleado.obtenerDetalles());

        if(empleado instanceof Escritor){
            Escritor escritor = (Escritor) empleado;
            resultado = escritor.getTipoEscrituraEnTexto();
            ((Escritor)empleado).tipoEscritura.getDescripcion();
            System.out.println("El resultado es: " +resultado);
            }else if(empleado instanceof Gerente){
            resultado = ((Gerente)empleado).getDepartamento();
            System.out.println("El departamento es: "+resultado);
        }
        }

    }

