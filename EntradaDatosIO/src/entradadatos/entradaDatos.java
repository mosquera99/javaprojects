package entradadatos;
import java.io.*;

public class entradaDatos {
    public static void main(String[] args) {

        InputStreamReader inputStream = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(inputStream);
        try {
            System.out.println("Intoduce un dato");
            String campo = reader.readLine();
            while (campo != null){
                System.out.println("Valor campo: "+campo);
                System.out.println("\n");
                System.out.println("Intoduce un dato");
                campo = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
