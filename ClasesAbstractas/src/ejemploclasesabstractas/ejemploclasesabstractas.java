package ejemploclasesabstractas;

import abstracto.domain.*;

public class ejemploclasesabstractas {

    public static void main(String[] args) {
        FiguraGeometrica circulo = new Circulo("Circulo");
        FiguraGeometrica triangulo = new Triangulo("Triangulo");
        FiguraGeometrica rectangulo = new Rectangulo("Rectangulo");

          System.out.println(circulo);
          circulo.dibujar();
          System.out.println(" ");

          System.out.println(triangulo);
          triangulo.dibujar();
          System.out.println(" ");

          System.out.println(rectangulo);
          rectangulo.dibujar();

    }
}
