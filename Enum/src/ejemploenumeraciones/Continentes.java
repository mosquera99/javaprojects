package ejemploenumeraciones;

public enum Continentes {

    AFRICA(54),
    AMERICA(34),
    OCEANIA(14),
    ASIA(44),
    EUROPA(46);

    private final int paises;

    Continentes (int paises){
        this.paises = paises;
    }

    public int getPaises() {
        return paises;
    }
}
