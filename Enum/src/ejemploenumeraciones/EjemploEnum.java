package ejemploenumeraciones;

public class EjemploEnum {

    public static void main(String[] args) {
        System.out.println("Continente de buena drug :" + Continentes.AMERICA);
        System.out.println("Estos son los paises de america :" + Continentes.AMERICA.getPaises());

        System.out.println("");
        indicarPaises(Continentes.AFRICA);
        indicarPaises(Continentes.ASIA);
        imprimirPaises();
    }

    private static void indicarPaises(Continentes continentes) {
        switch (continentes){
            case ASIA:
                System.out.println("Paises de el continente :"+continentes+" Con un numero de paises :"+continentes.getPaises());
                break;
            case AFRICA:
                System.out.println("Paises de el continente :"+continentes+" Con un numero de paises :"+continentes.getPaises());
                break;
            default:
                System.out.println("Seguimos con los demas contienentes");
                break;
        }
    }

    private static void imprimirPaises() {
        for (Continentes c: Continentes.values()) {
            System.out.println("El continente : "+c+" Tiene : "+c.getPaises()+" paises.");
        }
    }


}
