package propiedadessistema;

import java.util.*;

public class PropiedadesSistema {
    public static void main(String[] args) {

        Properties properties = System.getProperties();
        Enumeration enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()){
            String nombrePropiedad = (String) enumeration.nextElement();
            String valorPropiedad = properties.getProperty(nombrePropiedad);
            System.out.println("Llave :" + nombrePropiedad + "=" + valorPropiedad);
        }
    }
}
