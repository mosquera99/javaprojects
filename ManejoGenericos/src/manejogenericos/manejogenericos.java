package manejogenericos;

public class manejogenericos {

    public static void main(String[] args) {
        ClaseGenerica<Integer> genericaInt = new ClaseGenerica(12);
        genericaInt.obtenerTipo();

        System.out.println("\n");

        ClaseGenerica<String> genericaString = new ClaseGenerica<String>("Prueba");
        genericaString.obtenerTipo();
    }
}
