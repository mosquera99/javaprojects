package manejogenericos;

public class ClaseGenerica<T> {

    private T object;

    public ClaseGenerica(T object){
        this.object = object;
    }

    public void obtenerTipo(){
        System.out.println("El tipo T es: " + object.getClass().getName());
    }
}
