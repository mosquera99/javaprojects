package ejemplopolimorfismo;

public class Main {

    public static void main(String[] args) {

        Empleado empleado = new Empleado("Luis",300000);
        imprimirInfo(empleado);

        Gerente gerente = new Gerente("Abraham",439890,"Antioquia");
        imprimirInfo(gerente);
    }

    public static void imprimirInfo(Empleado emp){
        System.out.println(emp.obtenerInfoEmpleado());
    }
}
