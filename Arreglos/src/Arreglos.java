public class Arreglos {

    public static void main(String[] args) {
        int [] numeros;
        numeros = new int[5];
        numeros[0] = 2;
        numeros[1] = 5;
        numeros[2] = 4;
        numeros[3] = 6;
        numeros[4] = 9;

        System.out.println("Tipo primitivo");
        System.out.println("Resultado indice 0: "+numeros[0]);
        System.out.println("Resultado indice 1: "+numeros[1]);
        System.out.println("Resultado indice 2: "+numeros[2]);
        System.out.println("Resultado indice 3: "+numeros[3]);
        System.out.println("Resultado indice 4: "+numeros[4]);
        System.out.println("");

        Persona [] personas;
        personas = new Persona[5];
        personas[0] = new Persona("Luis");
        personas[1] = new Persona("Hernan");
        personas[2] = new Persona("Henrique");
        personas[3] = new Persona("Armando");
        personas[4] = new Persona("Hector");

        System.out.println("Tipo Object");
        System.out.println("Resultado indice 0: "+personas[0]);
        System.out.println("Resultado indice 1: "+personas[1]);
        System.out.println("Resultado indice 2: "+personas[2]);
        System.out.println("Resultado indice 3: "+personas[3]);
        System.out.println("Resultado indice 4: "+personas[4]);
        System.out.println("");

        System.out.println("Arreglo con for");
        String gustos[] = {"Hierba","Papas","GtaV","Develop"};
        for (int i=0; i<gustos.length; i++){
            System.out.println("El indice :"+i+" Tiene el siguiente valor: "+gustos[i]);
        }
    }


}
