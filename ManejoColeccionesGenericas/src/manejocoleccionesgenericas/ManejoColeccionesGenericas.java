package manejocoleccionesgenericas;

import java.util.*;

public class ManejoColeccionesGenericas {
    public static void main(String[] args) {

        List<String> milista = new ArrayList<>();
        milista.add("Luis");
        milista.add("Armando");
        milista.add("Abraham");
        milista.add("Rene");
        imprimir(milista);

        Set<String> miSet = new HashSet<>();
        miSet.add("1");
        miSet.add("2");
        miSet.add("3");
        miSet.add("4");
        imprimir(miSet);

        Map<String,String> miMap = new HashMap<>();
        miMap.put("1","1");
        miMap.put("2","12");
        miMap.put("3","13");
        imprimir(miMap.keySet());
        imprimir(miMap.values());
    }

    private static void imprimir(Collection<String> col) {
        for (Object elemento : col) {
            System.out.println(elemento + " ");
        }

        System.out.println("\n");
    }
}
