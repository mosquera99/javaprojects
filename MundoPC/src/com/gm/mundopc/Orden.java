package com.gm.mundopc;

public class Orden {

    private  final int idOrden;
    private  final Computadora computadoras[];
    private static int contadorOrdenes;
    private int contadorComputadores;
    private static final int MAX_COMPUTADORAS  = 10;

    public Orden(){
        this.idOrden = ++contadorOrdenes;
        computadoras = new Computadora[MAX_COMPUTADORAS];
    }

    public void agregarComputadora(Computadora computadora){
        if(contadorComputadores<MAX_COMPUTADORAS){
            computadoras[contadorComputadores++] = computadora;
        }else{
            System.out.println("Se ha superado el maximo de productos en la orden "+MAX_COMPUTADORAS);
        }
    }

    public void mostrarOrden(){
        System.out.println("Orden #:"+idOrden);
        System.out.println("Computadora de la orden #:" + idOrden + ":");
        for (int i = 0; i <contadorComputadores ; i++) {
            System.out.println(computadoras[i]);
        }
    }


}
