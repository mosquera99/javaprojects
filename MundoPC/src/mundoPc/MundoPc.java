package mundoPc;

import com.gm.mundopc.*;

public class MundoPc {

    public static void main(String[] args) {
        //Computadora 1
        Monitor monitor = new Monitor("Acer",29);
        Raton raton = new Raton("USB","LG");
        Teclado teclado = new Teclado("USB","ASPIRE");
        Computadora computadora = new Computadora("Lenevo",teclado,monitor,raton);

        //Computadora 2
        Monitor monitor2 = new Monitor("LG",12);
        Raton raton2 = new Raton("USB","LG");
        Teclado teclado2 = new Teclado("USB","LG");
        Computadora computadora2 = new Computadora("LG",teclado2,monitor2,raton2);

        Computadora computadora3 = new Computadora("Armada",teclado2,monitor,raton);

        Orden orden = new Orden();
        orden.agregarComputadora(computadora);
        orden.agregarComputadora(computadora2);
        orden.mostrarOrden();

        Orden orden1 = new Orden();
        orden1.agregarComputadora(computadora);
        orden1.agregarComputadora(computadora3);
        System.out.println("");
        orden1.mostrarOrden();
    }
}
