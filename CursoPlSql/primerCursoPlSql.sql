
--Primer ejercicio plSlq DBMS_OUTPUTx
declare
vcadena varchar2(100) := 'Mi primer varchar2 plSql';
vcadena2 varchar2(100) := 'Mi segundo varchar2 plSql';
begin
   begin
      DBMS_OUTPUT.PUT_LINE(vcadena);
    exception
      when others then
        DBMS_OUTPUT.PUT_LINE('Fallo la impresión del vcadena');
   end; 
    begin  
      DBMS_OUTPUT.PUT_LINE(vcadena2);
    exception
      when others then
        DBMS_OUTPUT.PUT_LINE('Fallo la impresión del vcadena2');
    end;
  exception
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Fallo la impresión del vcadena2');
end;

--Segundo ejercicio plSlq DBMS_OUTPUT Tipos datos
declare
vNumber    number(16,2) := 15.45;
vDate      date := '15-08-2019';
vChar      char(20):= 'MYCHAR';
vVarchar   varchar2(20):= 'MY_VARCHAR';
vflag   boolean := FALSE;
begin
  DBMS_OUTPUT.PUT_LINE('El vNumber es: '|| vNumber);
  DBMS_OUTPUT.PUT_LINE('La vDate es: '|| vDate);
  DBMS_OUTPUT.PUT_LINE('El vChar es: '|| vChar);
  DBMS_OUTPUT.PUT_LINE('El vVarchar es: '|| vVarchar);
  --DBMS_OUTPUT.PUT_LINE('El vBoolean es: '|| vflag);
    exception 
      WHEN others then
        DBMS_OUTPUT.PUT_LINE('Error segundo ejercicio plSql');
END;

--Tercer ejecicio plSql If-Else-ElsIf
declare
verdadero boolean := true;
falso boolean := false;
begin
  if falso then
    SYS.DBMS_OUTPUT.PUT_LINE('Esta mierda si es verdadero');
  else
    SYS.DBMS_OUTPUT.PUT_LINE('Esta mierda si es falsa');
  end if;  
exception when 
  others then
    SYS.DBMS_OUTPUT.PUT_LINE('Fallo la comparacion de variables');
end;

declare
num1 number := 8;
num2 number := 5;
begin
  if num1 = num2 then 
    SYS.DBMS_OUTPUT.PUT_LINE('Si son iguales');
  elsif num1 <> num2 then
    SYS.DBMS_OUTPUT.PUT_LINE('Son distintos');
  elsif num1 != num2 then 
    SYS.DBMS_OUTPUT.PUT_LINE('Son distintos !=');
  elsif num1 <= num2 then
    SYS.DBMS_OUTPUT.PUT_LINE('El primer numero es menor');
  elsif num1 >= num2 then 
    SYS.DBMS_OUTPUT.PUT_LINE('El primer numero es mayor');
  else
    SYS.DBMS_OUTPUT.PUT_LINE('No se cumplio ni chimba');
  end if;
 exception 
  when others then 
    SYS.DBMS_OUTPUT.PUT_LINE('Fallo la segunda comparación');
end;


declare 
nombre1 VARCHAR2(100) := 'ABRAHAM2';
nombre2 VARCHAR2(100) := 'ABRAHAM';
begin
  if nombre1 = nombre2 then
    SYS.DBMS_OUTPUT.PUT_LINE('Los varchar2 son iguales');
  elsif nombre1 <> nombre2 then
    SYS.DBMS_OUTPUT.PUT_LINE('Los varchar2 son dsitintos');
  else
    SYS.DBMS_OUTPUT.PUT_LINE('No comparo ningun puto varchar2');
  end if;
 exception 
  when others then
    SYS.DBMS_OUTPUT.PUT_LINE('Fallo la tercera comparación');
end;  

--Cuarto ejercicio PlSql Case
SELECT JOB_TITLE,MAX_SALARY,MIN_SALARY, case when MIN_SALARY <= 9000 then 'Es muy bajo' else 'Es muy alto' end  as analisis FROM JOBS
order by MIN_SALARY desc;

select JOB_ID,case when DEPARTMENT_ID = 90 then 'Estos son los del departamento 90' end as Description from employees
where DEPARTMENT_ID in (90);

--Quinto ejecicio PlSql For
declare
contador number := 0;
numero1 number :=1;
numero2 number :=1;
begin
for n in numero1..numero2 
loop
  DBMS_OUTPUT.PUT_LINE(contador);
  contador := contador + 1;
end loop;
exception
  when others then
    DBMS_OUTPUT.PUT_LINE('Error en el for loop plSql');
end;

DECLARE
BEGIN
FOR i IN (SELECT EMPLOYEE_ID,FIRST_NAME,SALARY FROM EMPLOYEES)
LOOP
  DBMS_OUTPUT.PUT_LINE(i.EMPLOYEE_ID||' '||i.FIRST_NAME ||' '|| i.SALARY);
END LOOP;
  EXCEPTION
     WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Fallo el for loop con el Select');
END;

--Function decode
select FIRST_NAME,DECODE(DEPARTMENT_ID,100,'Finance',
                                       90,'Executive',
                                       60,'IT',
                                       'No tiene departamento') Decode from EMPLOYEES
where DEPARTMENT_ID in (100,60,90) 
order by decode;

--Sexto ejecicio PlSlq While