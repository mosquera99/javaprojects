package test;

import  static org.junit.Assert.*;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import org.junit.Before;
import org.junit.Test;
import beans.HolaMundoEJB;

public class HolaMundoEJBTest {

    private static EJBContainer container;
    private static Context context;
    private static  HolaMundoEJB ejb;
    @Before
    public void iniciaContenedor() throws Exception{
        System.out.println("Inicia el contenedor");
        container = EJBContainer.createEJBContainer();
        context = container.getContext();
        ejb = (HolaMundoEJB) context.lookup("java:global/classes/HolaMundoEJB!beans.HolaMundoEJB");
    }

    @Test
    public void testAddNumber(){
        int dato1 = 7;
        int dato2 = 6;
        int resultado = ejb.sumar(dato1,dato2);
        assertEquals(dato1+dato2,resultado);
        System.out.println("\n Operación terminada el resultado es: "+resultado+"\n");
    }



}
