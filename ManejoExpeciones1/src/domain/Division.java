package domain;

public class Division {

    private int numerador;
    private int demoninador;

    public Division(int numerador,int demoniador) throws OperationException {
        if(demoniador == 0){
            throw  new OperationException("No se puede dividir por cero");
        }
        this.numerador = numerador;
        this.demoninador = demoniador;
    }

    public void obtenerDivision(){
        System.out.println("El resultado de la división es:" +   numerador/demoninador );
    }
}
