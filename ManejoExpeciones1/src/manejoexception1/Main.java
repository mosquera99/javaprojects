package manejoexception1;

import domain.*;

public class Main {
    public static void main(String[] args) {

        try {
            Division  division = new Division(0,9);
            division.obtenerDivision();
        } catch (OperationException e) {
            System.out.println("Ocurrio un error");
            e.printStackTrace();
        }
    }
}
