package manejoexception1;
import domain.*;

public class ManejoExceptionesArgs {

    public static void main(String[] args) throws OperationException {
        try {
            int op1 = Integer.parseInt(args[0]);
            int op2 = Integer.parseInt(args[1]);
            Division division = new Division(op1,op2);
            division.obtenerDivision();
        }catch (ArrayIndexOutOfBoundsException aie){
            System.out.println("Ocuerrio una exception");
            System.out.println("Se a proporccionado un elemento fuera de rango");
            aie.printStackTrace();
        }catch (NumberFormatException e) {
            System.out.println("Ocuerrio una exception");
            System.out.println("No se a proporcionado un dato numerico");
            e.printStackTrace();
        }finally {
            System.out.println("Se acaba la ejecucción");
        }

    }
}
