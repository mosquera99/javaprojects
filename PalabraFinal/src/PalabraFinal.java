public final class PalabraFinal {

    //Variables marcadas como finales
    public static final int VAR_PRMITIVO = 10;

    public static final Persona VAR_PERSONA = new Persona();

    public final void metodoFinal(){

    }
}

/*class PalabraFinal2 extends PalabraFinal {
    public final void metodoFinal(){

    }*/


