public class ImplPalabraFinal {

    public static void main(String[] args) {
        //Modificar un atributo final,no es posible
        //PalabraFinal.VAR_PRMITIVO = 20;

        //Modificar la referencia de un atributo  de tipo object,no es posible
        //PalabraFinal.VAR_PERSONA = new Persona();

        PalabraFinal.VAR_PERSONA.setNombre("ARMANDO");
        System.out.println(PalabraFinal.VAR_PERSONA.getNombre());
        PalabraFinal.VAR_PERSONA.setNombre("HENRIQUITO");
        System.out.println(PalabraFinal.VAR_PERSONA.getNombre());
    }
}
