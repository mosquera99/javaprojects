package manejointerface;

import datos.*;

public class manejointerface {

    public static void main(String[] args) {

        AccesoDatos datos = new ImplementacionOracle();
        ejecutar(datos,"listar");
        ejecutar(datos,"registrar");

        datos = new ImplementacionMySql();
        ejecutar(datos,"registrar");

    }

    private static void ejecutar(AccesoDatos datos, String accion) {
        if ("listar".equals(accion)){
            datos.listarValores();
        }else if("registrar".equals(accion)){
            datos.registraValores();
        }
    }
}
