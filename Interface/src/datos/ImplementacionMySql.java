package datos;

public class ImplementacionMySql implements AccesoDatos {
    @Override
    public void listarValores() {
        System.out.println("Listar valores desde MySql");
    }

    @Override
    public void registraValores() {
        System.out.println("Registrar valores desde MySql");
    }
}
