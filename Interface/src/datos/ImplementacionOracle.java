package datos;

public class ImplementacionOracle implements AccesoDatos  {

    @Override
    public void listarValores() {
        System.out.println("Listar valores desde Oralce");
    }

    @Override
    public void registraValores() {
        System.out.println("Registrar valores desde Oralce");
    }
}
