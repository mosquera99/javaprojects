package datos;

public interface AccesoDatos {

     int MAX_REGISTROS = 10;

     void  listarValores();
     void  registraValores();
}
