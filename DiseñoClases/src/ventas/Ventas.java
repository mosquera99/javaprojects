package ventas;

import com.co.diseñoClases.*;

public class Ventas {
    public static void main(String[] args) {
        Producto producto1 = new Producto("Camisa",2000);
        Producto producto2 = new Producto("Relog",4000);

        Orden orden = new Orden();
        orden.agragarProducto(producto1);
        orden.agragarProducto(producto2);

        Producto producto3 = new Producto("Saco",50000);
        Producto producto4 = new Producto("Manilla",5000);

        Orden orden1 = new Orden();
        orden1.agragarProducto(producto3);
        orden1.agragarProducto(producto4);

        orden1.mostrarOrden();

    }
}
