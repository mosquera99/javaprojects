package ejemploSobrescritura;

public class Main {

    public static void main(String[] args) {

        Empleado empleado = new Empleado("Serafin",1.560000);
        System.out.println("Empleado: " + empleado.obtenerInfo());

        System.out.println(" ");

        Gerente gerente= new Gerente("Abraham",4.500000,"Antioquia");
        System.out.println("Gerente: " + gerente.obtenerInfo());

    }
}
