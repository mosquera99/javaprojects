package test;

import mx.com.gm.sga.domain.Persona;
import mx.com.gm.sga.servicio.PersonaService;
import javax.ejb.embeddable.EJBContainer;
import org.junit.Test;
import org.junit.Before;

import java.util.List;

import static org.junit.Assert.*;


public class PersonaServiceTest {

    private PersonaService personaService;

    @Before
    public void setUp () throws Exception{
        EJBContainer contenedor = EJBContainer.createEJBContainer();
        personaService = (PersonaService)
                contenedor.getContext().lookup("java:global/sga-jee/PersonaServiceImpl!mx.com.gm.sga.servicio.PersonaService");
    }

    @Test
    public void testEJBPersonaService(){
        System.out.println("Inicia el llamado al EJB Local PersonaService \n");
        assertTrue(personaService != null);
        assertEquals(2,personaService.listarPersonas().size());

        System.out.println("El no de personas es igual a: "+ personaService.listarPersonas().size());
        this.desplegarPersonas(personaService.listarPersonas());

    }

    private void desplegarPersonas(List<Persona> listarPersonas) {
        for (Persona p: listarPersonas){
            System.out.println(p);
        }
    }

}
