package mx.com.gm.sga.cliente;

import mx.com.gm.sga.domain.Persona;
import mx.com.gm.sga.servicio.PersonaServiceRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

public class ClientePersonaService {

    public static void main(String[] args) {

        System.out.println("Inicia llamada a ejb \n");
        try{
            Context jndi = new InitialContext();
            PersonaServiceRemote personaServiceRemote =
                    (PersonaServiceRemote) jndi.lookup("java:global/sga-jee/PersonaServiceImpl!mx.com.gm.sga.servicio.PersonaServiceRemote");

            List<Persona> personas = personaServiceRemote.listarPersonas();
            for (Persona p: personas){
                System.out.println(p);
            }
            System.out.println("\n Finaliza llamda al ejb desde el cliente");
        }catch (NamingException e) {
            e.printStackTrace();
        }
    }

}
