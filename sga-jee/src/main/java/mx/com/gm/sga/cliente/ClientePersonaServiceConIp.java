package mx.com.gm.sga.cliente;

import mx.com.gm.sga.domain.Persona;
import mx.com.gm.sga.servicio.PersonaServiceRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;
import java.util.Properties;

public class ClientePersonaServiceConIp {

    public static void main(String[] args) {

            System.out.println("Inicia llamada a ejb \n");
            try{
                Properties properties =  new Properties();

                properties.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
                properties.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");
                properties.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
                properties.setProperty("org.omg.CORBA.ORBInitialHost", "127.0.0.1");
                //Propertie optional
                //properties.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

                Context jndi = new InitialContext(properties);
                PersonaServiceRemote personaServiceRemote =
                        (PersonaServiceRemote) jndi.lookup("java:global/sga-jee/PersonaServiceImpl!mx.com.gm.sga.servicio.PersonaServiceRemote");

                List<Persona> personas = personaServiceRemote.listarPersonas();
                for (Persona p: personas){
                    System.out.println(p);
                }
                System.out.println("\n Finaliza llamda al ejb desde el cliente");
            }catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

