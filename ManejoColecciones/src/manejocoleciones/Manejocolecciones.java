package manejocoleciones;

import java.util.*;
public class Manejocolecciones {
    public static void main(String[] args) {

        List milista = new ArrayList();
        milista.add("1");
        milista.add("2");
        milista.add("3");
        milista.add("4");
        milista.add("5");
        imprimir(milista);

        Set miHash = new HashSet();
        miHash.add("200");
        miHash.add("300");
        miHash.add("400");
        miHash.add("200");
        imprimir(miHash);

        Map miMap = new HashMap();
        miMap.put("1","Juan");
        miMap.put("2","Luis");
        miMap.put("3","Armando");
        imprimir(miMap.keySet());
        imprimir(miMap.values());
    }

    private static void imprimir(Collection coleccion) {
        for (Object object : coleccion) {
            System.out.println("Elemento: " + object);
        }

        System.out.println(" ");
    }
}
