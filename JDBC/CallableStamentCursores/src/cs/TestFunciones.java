package cs;

import  java.sql.*;
import Datos.Conexion;

public class TestFunciones {

    public static void main(String[] args) {
        int employe_id = 100;
        StringBuilder builder = new StringBuilder();
        try{
           Connection connection = Conexion.getConection();
           CallableStatement callableStatement = null;
           callableStatement = connection.prepareCall("{ ? = call get_employee_salary(?) }");
           double salario_mensual;
           callableStatement.registerOutParameter(1,Types.INTEGER);
           callableStatement.setInt(2,employe_id);
           callableStatement.execute();
           salario_mensual = callableStatement.getDouble(1);
           callableStatement.close();
            builder.append("Empleado con id: ");
            builder.append(employe_id);
            builder.append("\n");
            builder.append("Salario $: ");
            builder.append(salario_mensual);
            System.out.println(builder);
        } catch (SQLException e) {
            System.out.println("Fallo el llamado a la función");
            e.printStackTrace();
        }
    }
}
