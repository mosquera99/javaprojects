package cs;

import java.sql.*;
import oracle.jdbc.*;
import Datos.Conexion;

public class TestCursores {
    public static void main(String[] args) {
        OracleCallableStatement oraCallStmt = null;
        OracleResultSet oraRS = null;
        try {
            Connection conn = Conexion.getConection();
         oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call ref_cursor_package.get_dept_ref_cursor(?)}");
         oraCallStmt.registerOutParameter(1,OracleTypes.CURSOR);
         oraCallStmt.setInt(2,200);
         oraCallStmt.execute();

         oraRS = (OracleResultSet) oraCallStmt.getCursor(1);
         while (oraRS.next()){
             System.out.print(" El id del departamento es: " + oraRS.getInt(1));
             System.out.print(", El nombre del departamento es: " + oraRS.getString(2));
             System.out.print(", La locacion del departamento es: " + oraRS.getInt(3));
             System.out.println();
         }
         oraCallStmt.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
