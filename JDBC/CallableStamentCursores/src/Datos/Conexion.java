package Datos;

import java.util.*;
import java.sql.*;

public class Conexion {

    private static  String JDBC_DRIVER;
    private static  String JDBC_URL;
    private static  String JDBC_USER;
    private static  String JDBC_PASSORD;
    private static  Driver driver;

    private static Properties loadProperties(String file){
        Properties properties = new Properties();
        ResourceBundle bundle = ResourceBundle.getBundle(file);
        Enumeration e = bundle.getKeys();
        String key = null;
        while (e.hasMoreElements()){
            key = (String) e.nextElement();
            properties.put(key,bundle.getObject(key));
        }

        JDBC_DRIVER = properties.getProperty("driver");
        JDBC_URL = properties.getProperty("url");
        JDBC_USER = properties.getProperty("user");
        JDBC_PASSORD = properties.getProperty("password");

        return properties;
    }

    public static  synchronized Connection getConection() throws SQLException{
        if (driver==null){
            try {
                loadProperties("jdbc");
                Class jdbcDriverClass = Class.forName(JDBC_DRIVER);
                driver = (Driver) jdbcDriverClass.newInstance();
                DriverManager.registerDriver(driver);
            }catch (Exception e){
                System.out.println("Fallo la carga del driver del jdbc");
                e.printStackTrace();
            }
        }
        return DriverManager.getConnection(JDBC_URL,JDBC_PASSORD,JDBC_USER);
    }

    public static void close(ResultSet set) {
        try {
            if (set != null) {
                set.close();
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }


    public static void close(PreparedStatement pared){
        try{
            if (pared != null) {
                pared.close();
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }


    public static void close(Connection conn){
        try{
            if (conn != null) {
                conn.close();
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }
}
