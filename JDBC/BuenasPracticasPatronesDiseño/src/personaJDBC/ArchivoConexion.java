package personaJDBC;

import java.sql.*;
public class ArchivoConexion {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://localhost/sga?useSSL=false";
    public static final String JDBC_USER = "root";
    public static final String JDBC_PASS = "admin";
    public static Driver driver = null;

    public static synchronized Connection getConnection() throws SQLException{
        try {
            if(driver==null){
                Class jdbcDriverClass = Class.forName(JDBC_DRIVER);
                driver = (Driver) jdbcDriverClass.newInstance();
            }
        }catch (Exception e){
            System.out.println("Fallo la carga del driver");
            e.printStackTrace();
        }

        return DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASS);
    }

    public static void closeRS(ResultSet rs){
        try{
            if(rs != null){
                rs.close();
            }
        }catch (SQLException sqlR){
            sqlR.printStackTrace();
        }
    }

    public static void closePreparedStatement(PreparedStatement ps){
        try{
            if(ps != null){
                ps.close();
            }
        }catch (SQLException sqlP){
            sqlP.printStackTrace();
        }
    }

    public static void closeConnecion (Connection connection){
        try{
            if(connection != null){
                connection.close();
            }
        }catch (SQLException sqlC){
            sqlC.printStackTrace();
        }
    }
}
