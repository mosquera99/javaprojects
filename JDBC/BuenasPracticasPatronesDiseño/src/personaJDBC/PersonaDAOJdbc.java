package personaJDBC;

import personaDTO.PersonaDTO;

import java.sql.*;
import java.util.*;

public class PersonaDAOJdbc implements PersonaDAO {

    private final String SQL_INSERT = "insert into persona (nombre,apellido) values (?,?)";
    private final String SQL_UPDATE = "update persona set nombre=?,apellido=? where id_persona = ?";
    private final String SQL_DELETE = "delete from persona where id_persona = ?";
    private final String SQL_SELECT = "select id_persona,nombre,apellido from persona";
    private Connection userConec = null;

    public PersonaDAOJdbc() {
    }

    public PersonaDAOJdbc(Connection userConec) {
        this.userConec = userConec;
    }

    @Override
    public int insert(PersonaDTO personaDTO) throws SQLException {
        Connection con = null;
        PreparedStatement pst = null;
        int rows = 0;
        try {
            int index = 1;
            con = (this.userConec!=null) ? this.userConec :
                ArchivoConexion.getConnection();
            pst = con.prepareStatement(SQL_INSERT);
            System.out.println("Ejecutando insert: " + SQL_INSERT);
            pst.setString(index++,personaDTO.getNombre());
            pst.setString(index,personaDTO.getApellido());
            rows = pst.executeUpdate();
            System.out.println("Numero de registros insertados: " + rows);
        }finally {
             ArchivoConexion.closePreparedStatement(pst);
             if(this.userConec == null){
                 ArchivoConexion.closeConnecion(con);
             }


        }
        return rows;
    }

    @Override
    public int update(PersonaDTO personaDTO) throws SQLException {
        Connection conne = null;
        PreparedStatement pstm = null;
        int rows = 0;
        try {
            int index = 1;
            conne = (this.userConec !=null) ? this.userConec :
                    ArchivoConexion.getConnection();
            pstm = conne.prepareStatement(SQL_UPDATE);
            System.out.println("Ejecutando update: " + SQL_UPDATE);
            pstm.setString(index++,personaDTO.getNombre());
            pstm.setString(index++,personaDTO.getApellido());
            pstm.setInt(index++,personaDTO.getId_persona());
            rows = pstm.executeUpdate();
            System.out.println("Numero de registros modificados: " + rows);
        }finally {
            ArchivoConexion.closePreparedStatement(pstm);
            if (this.userConec == null){
                ArchivoConexion.closeConnecion(conne);
            }
        }
        return rows;
    }

    @Override
    public int delete(PersonaDTO personaDTO) throws SQLException {
        Connection cnn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        try {
            cnn = (this.userConec !=null) ? this.userConec :
                    ArchivoConexion.getConnection();
            stmt = cnn.prepareStatement(SQL_DELETE);
            System.out.println("Ejecutando delete: " + SQL_DELETE);
            stmt.setInt(1,personaDTO.getId_persona());
            rows = stmt.executeUpdate();
            System.out.println("Numero de registros eliminados: " + rows);
        }finally {
                ArchivoConexion.closePreparedStatement(stmt);
            if (this.userConec ==  null){
                ArchivoConexion.closeConnecion(cnn);
            }
        }
        return rows;
    }

    @Override
    public List<PersonaDTO> select() throws SQLException {
        Connection connection = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        PersonaDTO personaDTO = null;
        List<PersonaDTO> personas = new ArrayList<>();
        try {
            connection = (this.userConec != null) ? this.userConec :
                    ArchivoConexion.getConnection();
            psmt = connection.prepareStatement(SQL_SELECT);
            System.out.println("Ejecutando select: " + SQL_SELECT);
            rs = psmt.executeQuery();
            while (rs.next()){
                personaDTO = new PersonaDTO();
                personaDTO.setId_persona(rs.getInt(1));
                personaDTO.setNombre(rs.getString(2));
                personaDTO.setApellido(rs.getString(3));
                personas.add(personaDTO);
            }
        }finally {
            ArchivoConexion.closeRS(rs);
            ArchivoConexion.closePreparedStatement(psmt);
            if (this.userConec == null){
                ArchivoConexion.closeConnecion(connection);
            }
        }
        return personas;
    }
}
