package personaJDBC;
import java.sql.*;
import java.util.List;
import personaDTO.PersonaDTO;

public interface PersonaDAO {

    int insert(PersonaDTO personaDTO) throws SQLException;
    int update(PersonaDTO personaDTO) throws SQLException;
    int delete(PersonaDTO personaDTO) throws SQLException;
    List<PersonaDTO> select() throws SQLException;

    }

