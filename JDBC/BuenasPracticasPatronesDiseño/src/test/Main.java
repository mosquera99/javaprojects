package test;


import personaDTO.PersonaDTO;
import personaJDBC.PersonaDAO;
import personaJDBC.PersonaDAOJdbc;

import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            PersonaDTO  personaDTO = new PersonaDTO();
            PersonaDAO personaDAO = new PersonaDAOJdbc();
            /*personaDTO.setNombre("Nico");
            personaDTO.setApellido("Herrra");
            personaDAO.insert(personaDTO);
            personaDTO.setId_persona(1);
            personaDAO.delete(personaDTO);*/
            personaDTO.setNombre("Cristiano");
            personaDTO.setApellido("Dos Santos");
            personaDTO.setId_persona(5);
            personaDAO.update(personaDTO);
            List<PersonaDTO> personasDTO = personaDAO.select();
            for (PersonaDTO perDTO: personasDTO) {
                    System.out.println(perDTO);
                    System.out.println(" ");
            }
        }catch (SQLException e){
            System.out.println("Expción Capa de prueba");
            e.printStackTrace();
        }
    }
}
