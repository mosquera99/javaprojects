package cs;

import Datos.Conexion;

import java.util.*;
import java.sql.*;

public class TestProcedimientos {

    public static void main(String[] args) {
        int empleado_id = 100;
        double incremento_salario = 1.1;
        Connection con = null;
        try{
            con = Conexion.getConection();
            Statement smts = null;
            ResultSet rset = null;
            CallableStatement csmt = null;

            smts = con.createStatement();

            System.out.println("Aumento del 10% del salrio al empleado: " + empleado_id);
            csmt = con.prepareCall("{call update_salary_employee(?,?)}");
            csmt.setInt(1,empleado_id);
            csmt.setDouble(2,incremento_salario);
            csmt.execute();
            csmt.close();

            String querySelect = "SELECT first_name,salary FROM employees "
                    + "WHERE employee_id = " + empleado_id;

            rset = smts.executeQuery(querySelect);
            rset.next();
            System.out.println("Nombre: " + rset.getString(1));
            System.out.println("Salario: " + rset.getFloat(2));
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

}
