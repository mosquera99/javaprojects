package introduccionjdbc;

import  java.sql.*;
public class IntroduccionJDBC {
    public static void main(String[] args) {

        String url = "jdbc:mysql://localhost:3306/sga?useSSL=false";
        try{
            //Cargamos el driver de mysql
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = (Connection) DriverManager.getConnection(url,"root","admin");
            //Creamos un stament
            Statement statement = connection.createStatement();
            //Creamos el query
            String sql = "select id_persona,nombre,apellido from persona";
            //Se crea el resulset en la obtencion de datos(No siempre es necesario)
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                System.out.println("Id: " + resultSet.getInt(1));
                System.out.println("Nombre: " + resultSet.getString(2));
                System.out.println("Apellido: " + resultSet.getString(3));
            }

            resultSet.close();
            statement.close();
            connection.close();
        }catch (SQLException | ClassNotFoundException ex){
            ex.printStackTrace();
        }
    }
}
