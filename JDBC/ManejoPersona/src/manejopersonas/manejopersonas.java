package manejopersonas;

import domain.Persona;
import  datos.PersonaJDBC;
import java.util.List;

public class manejopersonas {

    public static void main(String[] args) {
        PersonaJDBC personaJDBC = new PersonaJDBC();
        //personaJDBC.insert("Abraham","Mosquera");
        //personaJDBC.update(1,"Ricardo","Zapata");
        //personaJDBC.delete(3);

        List<Persona> personas = personaJDBC.select();
        for (Persona per : personas){
            System.out.println(per);
            System.out.println(" ");
        }
    }
}
