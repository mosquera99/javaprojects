package poolconexiones;

import capadatos.pool.PoolConexionesOracle;
import capadatos.pool.PoolConexionesMysql;
import java.sql.*;
public class TestPoolConexiones {

    public static void main(String[] args) {

        Connection connection = null;
        PreparedStatement pstatement = null;
        ResultSet rs = null;
        try{
            //Pool de conexiones Mysql
            connection = PoolConexionesMysql.getConexion();
            pstatement = connection.prepareStatement("SELECT * FROM USUARIO");
            System.out.println("Usando el Pool de conexiones de mysql");
            rs = pstatement.executeQuery();
            while (rs.next()){
                System.out.print("Id usuario: " + rs.getInt(1));
                System.out.print(" Nombre usuario: " + rs.getString(2));
                System.out.println(" Password usuario: " + rs.getString(3));
            }

            //Cerramos la conexiones la cual ya queda disponible para el pool de conexiones
            connection.close();

            System.out.println(" ");

            //Pool de conexiones de Oracle
            connection = PoolConexionesOracle.getConnection();
            pstatement = connection.prepareStatement("SELECT * FROM EMPLOYEES WHERE EMPLOYEE_ID IN (100,101,102)");
            System.out.println("Usando el Pool de conexiones de oracle");
            rs = pstatement.executeQuery();
            while (rs.next()){
                System.out.print("Id empleado: " + rs.getInt(1));
                System.out.print(" Nombre empleado: " + rs.getString(2));
                System.out.println(" Apellido empleado: " + rs.getString(3));
            }

            //Cerramos la conexiones la cual ya queda disponible para el pool de conexiones
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();

        }

    }
}
