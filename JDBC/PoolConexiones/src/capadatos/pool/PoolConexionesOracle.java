package capadatos.pool;

import  java.sql.*;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

public class PoolConexionesOracle {

    public static DataSource getDataSource(){
        BasicDataSource bs = new BasicDataSource();
        bs.setDriverClassName("oracle.jdbc.OracleDriver");
        bs.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
        bs.setUsername("hr");
        bs.setPassword("hr");
        //Definimos el tamaño del pool de conexiones
        bs.setInitialSize(5);
        return bs;
    }

    public static Connection getConnection() throws SQLException{
        return getDataSource().getConnection();
    }
}
