package capadatos.pool;

import  java.sql.*;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

public class PoolConexionesMysql {

    public static DataSource getDataSource(){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://localhost/sga?useSSL=false");
        ds.setUsername("root");
        ds.setPassword("admin");
        //Definimos el tamaño del pool de conexiones
        ds.setInitialSize(5);
        return ds;
    }

    public static Connection getConexion() throws SQLException{
        return getDataSource().getConnection();
    }
}
