package com.gm.SGAOracle.jdbc;

import java.sql.*;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

public class ResourceManager
{
    private static String JDBC_DRIVER;
    private static String JDBC_URL;
    private static String JDBC_USER;
    private static String JDBC_PASSWORD;
    private static String JDBC_INITSIZE;
    private static Driver driver = null;

    private static Properties loadProperties(String file){
    	Properties properties = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle(file);
		Enumeration e = bundle.getKeys();
		String key = null;
		while (e.hasMoreElements()){
			key = (String) e.nextElement();
			properties.put(key,bundle.getObject(key));
		}

		JDBC_DRIVER = properties.getProperty("driver");
		JDBC_URL = properties.getProperty("url");
		JDBC_USER = properties.getProperty("user");
		JDBC_PASSWORD = properties.getProperty("password");
		JDBC_INITSIZE = properties.getProperty("initsize");
		return  properties;
	}

    public static synchronized Connection getConnection() throws SQLException
    {
        if (driver == null)
        {
            try
            {
            	loadProperties("jdbc");
                Class jdbcDriverClass = Class.forName( JDBC_DRIVER );
                driver = (Driver) jdbcDriverClass.newInstance();
                DriverManager.registerDriver( driver );
            }
            catch (Exception e)
            {
                System.out.println( "Fallo la carga del driver JDBC" );
                e.printStackTrace();
            }
        }

        return DriverManager.getConnection(
                JDBC_URL,
                JDBC_USER,
                JDBC_PASSWORD
        );
    }


	public static void close(Connection conn)
	{
		try {
			if (conn != null) conn.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}
	}

	public static void close(PreparedStatement stmt)
	{
		try {
			if (stmt != null) stmt.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}
	}

	public static void close(ResultSet rs)
	{
		try {
			if (rs != null) rs.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}

	}

}
