package cs;
import java.sql.*;
import Datos.Conexion;
import oracle.jdbc.OracleResultSetMetaData;

public class TestMetadatos {
    public static void main(String[] args) {

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            con = Conexion.getConection();
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM EMPLOYEES");
            //Obtnenemos un objeto de metadatos de oracle
            OracleResultSetMetaData oracleRS = (OracleResultSetMetaData) rs.getMetaData();
            if(oracleRS==null){
                System.out.println("No hay metadatos de la tabla");
            }else{
            //Preguntamos cuantas columnas tiene la tabla de empleados
                int columnCount = oracleRS.getColumnCount();
                System.out.println("El numero de columnas de la tabla son: " +  columnCount);
                for (int i=1; i <=columnCount;i++){
                    //Mostramos el nombre de la columna
                    System.out.print("Nombre de la columna: " + oracleRS.getColumnName(i));

                    //Mostramos el tipo de dato de la columna
                    System.out.print(", Tipo de dato de la columna: " +  oracleRS.getColumnTypeName(i));

                    switch (oracleRS.isNullable(i)){
                        case OracleResultSetMetaData.columnNoNulls:
                            System.out.print(", No acepta nulos");
                            break;
                        case OracleResultSetMetaData.columnNullable:
                            System.out.print(", Acepta nulos");
                            break;
                        case OracleResultSetMetaData.columnNullableUnknown:
                            System.out.print(", Valor nulo desconocido");
                    }

                    System.out.println("");
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            Conexion.close(rs);
            Conexion.close(con);
        }
    }
}
