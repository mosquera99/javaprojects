package datos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import domain.Usuario;

public class UsuarioJDBC {

    private final String SQL_INSERT = "insert into usuario (usuario,password) values (?,?)";
    private final String SQL_UPDATE = "update usuario set usuario=?,password=? where id_usuario = ?";
    private final String SQL_DELETE = "delete from usuario where id_usuario = ?";
    private final String SQL_SELECT = "select id_usuario,usuario,password from usuario";


    public int insertar(String usuario,String password){
        Connection conn = null;
        PreparedStatement statement = null;
        int rows = 0;
        try{
            conn = Conexion.getConnection();
            statement = conn.prepareStatement(SQL_INSERT);
            int index = 1;
            System.out.println("Realizando insert: " + SQL_INSERT);
            statement.setString(index++,usuario);
            statement.setString(index++,password);
            rows = statement.executeUpdate();
            System.out.println("Numero de filas registradas: " + rows);
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            Conexion.close(statement);
            Conexion.close(conn);
        }
        return rows;
    }

    public int update(String usuario,String password,int id_usuario){
        Connection conne = null;
        PreparedStatement psmt = null;
        int rows = 0;
        try{
            conne = Conexion.getConnection();
            psmt = conne.prepareStatement(SQL_UPDATE);
            int index = 1;
            System.out.println("Realizando update: " + SQL_UPDATE);
            psmt.setString(index++, usuario);
            psmt.setString(index++, password);
            psmt.setInt(index++, id_usuario);
            rows = psmt.executeUpdate();
            System.out.println("Numero de filas actualizadas: " + rows);
        }catch (SQLException ex){
            ex.printStackTrace();
        }finally {
            Conexion.close(psmt);
            Conexion.close(conne);
        }
        return rows;
    }

    public int delete(int id_usuario){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int rows = 0;
        try {
            connection = Conexion.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE);
            System.out.println("Realizando delete: " + SQL_DELETE);
            preparedStatement.setInt(1,id_usuario);
            rows = preparedStatement.executeUpdate();
            System.out.println("Numero de filas borradas: " + rows);
        }catch (SQLException esql){
            esql.printStackTrace();
        }finally {
            Conexion.close(preparedStatement);
            Conexion.close(connection);
        }
        return rows;
    }

    public List<Usuario> select(){
        Connection conexion = null;
        PreparedStatement stat = null;
        ResultSet set = null;
        List<Usuario> usuarios = new ArrayList<>();
        Usuario usuario = null;
        try {
            conexion = Conexion.getConnection();
            stat = conexion.prepareStatement(SQL_SELECT);
            System.out.println("Realizando select: " + SQL_SELECT);
            set = stat.executeQuery();
            while (set.next()){
                usuario = new Usuario();
                usuario.setId_persona(set.getInt(1));
                usuario.setUsuario(set.getString(2));
                usuario.setPassword(set.getString(3));
                usuarios.add(usuario);
            }
        }catch (SQLException sqlEx){
            sqlEx.printStackTrace();
        }finally {
                Conexion.close(set);
                Conexion.close(stat);
                Conexion.close(conexion);
        }

        return usuarios;
    }


}
