package datos;

import com.mysql.jdbc.Driver;

import java.sql.*;

public class Conexion {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://localhost/sga?useSSL=false";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "admin";
    private static Driver driver = null;

    public static synchronized Connection getConnection() throws SQLException {
        if (driver == null) {
            try {
                Class jdbcDriverClass = Class.forName(JDBC_DRIVER);
                driver = (Driver) jdbcDriverClass.newInstance();
                DriverManager.registerDriver(driver);
            } catch (Exception e) {
                System.err.println("Fallo la carga del driver JDBC");
                e.printStackTrace();
            }
        }

        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
    }

    public static void close(ResultSet set) {
        try {
            if (set != null) {
                set.close();
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    public  static void close(PreparedStatement pre){
        try{
            if(pre != null){
                pre.close();
            }
        }catch (SQLException sql1){
            sql1.printStackTrace();
        }
    }

    public static void close(Connection conn){
        try{
            if (conn != null){
                conn.close();
            }
        }catch (SQLException sql2){
            sql2.printStackTrace();
        }
    }

}
