package datos;

import java.sql.*;

public class Conexion {

    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String JDBC_URL = "jdbc:mysql://localhost/sga?useSSL=false";
    public static final String JDBC_USER = "root";
    public static final String JDBC_PASS = "admin";
    public static Driver driver = null;

    public static synchronized Connection getConnection() throws SQLException{
        if(driver==null) {
            try {
                Class jdbcDriverClass = Class.forName(JDBC_DRIVER);
                driver = (Driver) jdbcDriverClass.newInstance();
                DriverManager.registerDriver(driver);
            } catch (Exception ex) {
                System.err.println("Fallo la carga del driver JDBC");
                ex.printStackTrace();
            }

        }
        return DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASS);
    }

    public static void close(ResultSet set) {
        try {
            if (set != null) {
                set.close();
            }
            }catch(SQLException ex){
                ex.printStackTrace();
            }
        }


    public static void close(PreparedStatement pared){
            try{
                if (pared != null) {
                    pared.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }


    public static void close(Connection conn){
            try{
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }
