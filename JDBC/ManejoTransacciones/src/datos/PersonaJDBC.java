package datos;

import java.sql.*;
import java.util.*;
import domain.Persona;

public class PersonaJDBC {

    private java.sql.Connection userConnection;
    private final String SQL_INSERT = "insert into persona (nombre,apellido) values (?,?)";
    private final String SQL_UPDATE = "update persona set nombre=?,apellido=? where id_persona = ?";
    private final String SQL_DELETE = "delete from persona where id_persona = ?";
    private final String SQL_SELECT = "select id_persona,nombre,apellido from persona";

    public PersonaJDBC(){

    }

    public PersonaJDBC(Connection conn){
        this.userConnection = conn;
    }

    public int insert (String nombre,String apellido) throws SQLException{
        Connection conexion = null;
        PreparedStatement statement = null;
        int rows = 0;
        try{
            conexion = (this.userConnection != null) ? this.userConnection :  Conexion.getConnection();
            statement = conexion.prepareStatement(SQL_INSERT);
            int index = 1;
            statement.setString(index++,nombre);
            statement.setString(index++,apellido);
            System.out.println("Realizando sql: " + SQL_INSERT);
            rows = statement.executeUpdate();
            System.out.println("Numero de filas registradas: " + rows);
        }finally {
            Conexion.close(statement);
            if(this.userConnection == null){
                Conexion.close(conexion);
            }
        }
        return rows;
    }

    public int update (int id_persona,String nombre,String apellido) throws SQLException{
        int rows=0;
        Connection connection = null;
        PreparedStatement stmt = null;
        try{
            connection = (this.userConnection != null) ? this.userConnection : Conexion.getConnection();
            stmt = connection.prepareStatement(SQL_UPDATE);
            int contador = 1;
            stmt.setString(contador++,nombre);
            stmt.setString(contador++,apellido);
            stmt.setInt(contador,id_persona);
            System.out.println("Realizando sql: " + SQL_UPDATE);
            rows = stmt.executeUpdate();
            System.out.println("Numero de filas actualizadas: " + rows);
        }finally {
            Conexion.close(stmt);
            if(this.userConnection==null){
                Conexion.close(connection);
            }
        }
        return rows;
    }

    public int delete(int id_persona) throws  SQLException{
        int rows=0;
        Connection conn = null;
        PreparedStatement statement = null;
        try{
            conn = (this.userConnection != null) ? this.userConnection : Conexion.getConnection();
            statement = conn.prepareStatement(SQL_DELETE);
            statement.setInt(1,id_persona);
            System.out.println("Realizando sql: "+ SQL_DELETE);
            rows = statement.executeUpdate();
            System.out.println("Numero de filas borradas :" + rows);
        }finally {
            Conexion.close(statement);
            if (this.userConnection == null){
                Conexion.close(conn);
            }
        }
        return rows;
    }

    public List<Persona> select () throws SQLException{
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet set = null;
        Persona persona = null;
        List<Persona> personas = new ArrayList<>();
        try{
            connect =  (this.userConnection != null) ? this.userConnection : Conexion.getConnection();
            preparedStatement = connect.prepareStatement(SQL_SELECT);
            System.out.println("Realizando sql: "+ SQL_SELECT);
            set = preparedStatement.executeQuery();

            while (set.next()){
                persona = new Persona();
                persona.setId_persona(set.getInt(1));
                persona.setNombre(set.getString(2));
                persona.setApellido(set.getString(3));
                personas.add(persona);
            }
        }finally {
            Conexion.close(set);
            Conexion.close(preparedStatement);
            if(this.userConnection==null){
                Conexion.close(connect);
            }
        }
        return personas;
    }

}
