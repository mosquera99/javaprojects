package manejopersonas;

import datos.Conexion;
import domain.Persona;
import  datos.PersonaJDBC;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class manejopersonas {

    public static void main(String[] args) {
        Connection connect = null;
        try {
            connect = Conexion.getConnection();
            //Se valida si la conexion esta en autoCommit y se setea a false el autoCommit
            if(connect.getAutoCommit()){
                connect.setAutoCommit(false);
            }

            PersonaJDBC personaJDBC = new PersonaJDBC(connect);
            personaJDBC.update(2,"Pinto","Aristizabal");
            personaJDBC.insert("Savier","Mosquera");
            List<Persona> personas = personaJDBC.select();
            for (Persona per : personas){
                System.out.println(per);
                System.out.println(" ");
            }

            //Se hace el commit a la bd
            System.out.println("Va a realizar el commit");
            connect.commit();
        }catch (SQLException e){
            System.out.println("Haciendo al rollback de la trensacción");
            try{
                System.out.println("Entrando al rollback de la trensacción");
                e.printStackTrace(System.out);
                //Se hace el rollback a la bd
                connect.rollback();
            }catch (SQLException e2){
                e2.printStackTrace(System.out);
            }
        }

        //personaJDBC.insert("Abraham","Mosquera");
        //personaJDBC.update(1,"Ricardo","Zapata");
        //personaJDBC.delete(3);

        /*List<Persona> personas = personaJDBC.select();
        for (Persona per : personas){
            System.out.println(per);
            System.out.println(" ");
        }*/
    }
}
