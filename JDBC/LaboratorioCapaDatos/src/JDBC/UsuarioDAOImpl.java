package JDBC;

import DTO.UsuarioDTO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAOImpl implements UsuarioDAO {

    private final String SQL_INSERT = "insert into usuario (usuario,password) values (?,?)";
    private final String SQL_UPDATE = "update usuario set usuario=?,password=? where id_usuario = ?";
    private final String SQL_DELETE = "delete from usuario where id_usuario = ?";
    private final String SQL_SELECT = "select id_usuario,usuario,password from usuario";
    private Connection userCon = null;

    public UsuarioDAOImpl() {
    }

    @Override
    public int insert(UsuarioDTO usuarioDTO) throws SQLException {
        Connection conn = null;
        PreparedStatement psmt = null;
        int rows = 0;
        try {
            int index= 1;
            conn = (this.userCon!=null) ? this.userCon :
            Conexion.getConection();
            psmt = conn.prepareStatement(SQL_INSERT);
            System.out.println("Ejecutando sentencia: " + SQL_INSERT);
            psmt.setString(index++,usuarioDTO.getUsuario());
            psmt.setString(index,usuarioDTO.getPassword());
            rows = psmt.executeUpdate();
            System.out.println("Numero de filas registradas: " + rows);
        }finally {
            Conexion.closeStament(psmt);
            if (this.userCon==null)
                Conexion.closeConection(conn);
        }
        return rows;
    }

    @Override
    public int update(UsuarioDTO usuarioDTO) throws SQLException {
        Connection connect = null;
        PreparedStatement ps = null;
        int rows = 0;
        try {
            int index = 1;
            connect = (this.userCon!=null) ? this.userCon :
            Conexion.getConection();
            ps = connect.prepareStatement(SQL_UPDATE);
            System.out.println("Ejecutando sentencia: " + SQL_UPDATE);
            ps.setString(index++,usuarioDTO.getUsuario());
            ps.setString(index++,usuarioDTO.getPassword());
            ps.setInt(index,usuarioDTO.getId_usuario());
            rows = ps.executeUpdate();
            System.out.println("Numero de filas modificadas: " + rows);
        }finally {
            Conexion.closeStament(ps);
            if (this.userCon == null){
                    Conexion.closeConection(connect);
            }
        }
        return rows;
    }

    @Override
    public int delete(UsuarioDTO usuarioDTO) throws SQLException {
        Connection con = null;
        PreparedStatement pstm = null;
        int rows = 0;
        try {
            con = (this.userCon!=null) ? this.userCon :
            Conexion.getConection();
            pstm = con.prepareStatement(SQL_DELETE);
            System.out.println("Ejecutando sentencia: " + SQL_UPDATE);
            pstm.setInt(1,usuarioDTO.getId_usuario());
            rows = pstm.executeUpdate();
            System.out.println("Numero de filas eliminadas: " + rows);
        }finally {
            Conexion.closeStament(pstm);
            if (this.userCon==null){
                Conexion.closeConection(con);
            }
        }
        return rows;
    }

    @Override
    public List<UsuarioDTO> select() throws SQLException {
        List<UsuarioDTO> usuarios  = new ArrayList<>();
        UsuarioDTO usuarioDTO = null;
        Connection conexion = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conexion = (this.userCon != null) ? this.userCon :
            Conexion.getConection();
            stmt = conexion.prepareStatement(SQL_SELECT);
            System.out.println("Ejecutando sentencia: " + SQL_SELECT);
            rs = stmt.executeQuery();
            while (rs.next()){
                usuarioDTO = new UsuarioDTO();
                usuarioDTO.setId_usuario(rs.getInt(1));
                usuarioDTO.setUsuario(rs.getString(2));
                usuarioDTO.setPassword(rs.getString(3));
                usuarios.add(usuarioDTO);
            }
        }finally {
            Conexion.closeResulSet(rs);
            Conexion.closeStament(stmt);
            if(this.userCon == null){
                Conexion.closeConection(conexion);
            }
        }
        return usuarios;
    }
}
