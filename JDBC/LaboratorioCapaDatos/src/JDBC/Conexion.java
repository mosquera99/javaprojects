package JDBC;

import java.sql.*;

public class Conexion {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://localhost/sga?useSSL=false";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "admin";
    private static Driver driver = null;

    public static synchronized Connection getConection() throws SQLException {
        if (driver==null){
            try {
                    Class jdbcClassDriver = Class.forName(JDBC_DRIVER);
                    driver = (Driver) jdbcClassDriver.newInstance();
                    DriverManager.registerDriver(driver);
            }catch (Exception ex){
                System.out.println("Fallo la carga del driver");
                ex.printStackTrace();
            }
        }
            return DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASS);
    }

    public static void closeConection(Connection connection){
        try {
            if (connection!=null) {
                connection.close();
            }
        }catch (SQLException sqlC){
            sqlC.printStackTrace();
        }
    }

    public static void closeStament(PreparedStatement preparedStatement){
        try {
            if(preparedStatement!=null){
                preparedStatement.close();
            }
        }catch (SQLException sqlP){
            sqlP.printStackTrace();
        }
    }

    public static void closeResulSet(ResultSet rs){
        try {
            if(rs!=null){
                rs.close();
            }
        }catch (SQLException sqlE){
            sqlE.printStackTrace();
        }
    }
}
