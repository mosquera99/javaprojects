package TEST;

import DTO.UsuarioDTO;
import JDBC.*;

import java.sql.SQLException;
import java.util.List;

public class TestMain {
    public static void main(String[] args){
        try{
        UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        /*usuarioDTO.setUsuario("Serafin");
        usuarioDTO.setPassword("123456");
        usuarioDAO.insert(usuarioDTO);
        usuarioDTO.setUsuario("Abraham");
        usuarioDTO.setPassword("676328");
        usuarioDTO.setId_usuario(2);
        usuarioDAO.update(usuarioDTO);*/
        usuarioDTO.setId_usuario(3);
        usuarioDAO.delete(usuarioDTO);
        List<UsuarioDTO> list = usuarioDAO.select();
        for(UsuarioDTO dto : list ){
            System.out.println(dto);
            System.out.println(" ");
        }
    }catch (SQLException ex){
            System.out.println("Exepcion en la capa que llama el metodo");
        }
    }
}
