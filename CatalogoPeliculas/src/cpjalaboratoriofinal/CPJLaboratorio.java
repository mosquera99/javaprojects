package cpjalaboratoriofinal;

import mx.com.gm.peliculas.negocio.*;
import java.util.Scanner;

public class CPJLaboratorio {

    private static final Scanner  scanner = new Scanner(System.in);
    private static int option = -1;
    private static final String  ARCHIVO = "C:\\pruebaJava\\peli.txt";
    private static final CatalogoPeliculas catalogoPeliculas = new CatalogoPeliculasImpl();

    public static void main(String[] args) {
        //Mientras la opcion elegida sea 0 se visualiza el menu
        while (option != 0){
            try{
                System.out.println("Elige una opción: \n1. - Iniciar catalogo de peliculas"
                 + "\n2. - Agregar Peliculas \n"
                 + "3. - Listar Peliculas \n"
                 + "4. - Buscar Pelicula \n"
                 + "0. - Salir");
                option = Integer.parseInt(scanner.nextLine());

                switch (option){
                    case 1 :
                        //1. Creamos el objeto que administra el catalogo de peliculas
                        catalogoPeliculas.iniciarArchivo(ARCHIVO);
                        break;
                    case 2 :
                        //2. agregar informacion archivo
                        System.out.println("Introduce el nombre de la pelicula a agregar");
                        String nombrePlicula = scanner.nextLine();
                        catalogoPeliculas.agregarPelicula(nombrePlicula,ARCHIVO);
                        break;
                    case 3 :
                        //3. listar catalogo completo
                        catalogoPeliculas.listarPelicula(ARCHIVO);
                        break;
                    case 4 :
                        //4. Buscar pelicula
                        System.out.println("Introduce el nombre de la pelicula a que deceas buscar");
                        String buscar = scanner.nextLine();
                        catalogoPeliculas.buscarPelicula(ARCHIVO,buscar);
                        break;
                    case 0 :
                        System.out.println("!Hasta pronto!");
                        break;
                     default:
                         System.out.println("Opción incorrecta");
                         break;
                }

                System.out.println("\n");
            }catch (Exception e) {
                System.out.println("Error!");
            }
        }
    }
}
