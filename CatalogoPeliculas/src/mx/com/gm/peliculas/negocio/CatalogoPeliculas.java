package mx.com.gm.peliculas.negocio;

public interface CatalogoPeliculas {

    public void agregarPelicula(String nombrePelicula,String nombreArchivo);
    public void listarPelicula(String nombreArchivo);
    public void buscarPelicula(String buscar,String nombreArchivo);
    public void iniciarArchivo(String nombreArchivo);
}
