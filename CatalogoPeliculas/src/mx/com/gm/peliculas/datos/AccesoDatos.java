package mx.com.gm.peliculas.datos;

import mx.com.gm.peliculas.domain.Pelicula;
import mx.com.gm.peliculas.excepciones.*;

import java.util.List;

public interface AccesoDatos {

    boolean existe(String nombreArchivo) throws AccesoDatosEx;
    public List<Pelicula> listarPliculas(String nombreArchivo) throws LecturaDatosEx;
    void escribir (Pelicula pelicula,String nombreArchivo,boolean anexar) throws EscrituraDatosEx;
    public String buscar (String nombreArchivo,String buscar) throws LecturaDatosEx;
    public void crear(String nombreArchivo) throws AccesoDatosEx;
    public void borrar(String nombreArchivo) throws AccesoDatosEx;
}
