package mx.com.gm.peliculas.datos;

import mx.com.gm.peliculas.domain.Pelicula;
import mx.com.gm.peliculas.excepciones.AccesoDatosEx;
import mx.com.gm.peliculas.excepciones.EscrituraDatosEx;
import mx.com.gm.peliculas.excepciones.LecturaDatosEx;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AccesoDtosImpl implements AccesoDatos {
    @Override
    public boolean existe(String nombreArchivo) throws AccesoDatosEx {
        File arhcivo = new File(nombreArchivo);
        return arhcivo.exists();
    }

    @Override
    public List<Pelicula> listarPliculas(String nombreArchivo) throws LecturaDatosEx {
        File arhcivo = new File(nombreArchivo);
        List<Pelicula> peliculaList = new ArrayList();
        try{
            BufferedReader buffer = new BufferedReader(new FileReader(arhcivo));
            String linea = null;
            linea = buffer.readLine();
            while (linea != null){
                Pelicula pelicula = new Pelicula(linea);
                peliculaList.add(pelicula);
                linea = buffer.readLine();
            }
            buffer.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return peliculaList;
    }

    @Override
    public void escribir(Pelicula pelicula, String nombreArchivo, boolean anexar) throws EscrituraDatosEx {
        File arhcivo = new File(nombreArchivo);
        try{
            PrintWriter writer = new PrintWriter(new FileWriter(arhcivo));
            writer.println(pelicula.toString());
            writer.close();
            System.out.println("Se ha escrito correctamente al archivo");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String buscar(String nombreArchivo,String buscar) throws LecturaDatosEx {
        File arhcivo = new File(nombreArchivo);
        String resulado = null;
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(arhcivo));
            String linea = null;
            int i = 0;
            linea = bufferedReader.readLine();
            while(linea != null){
                if (buscar != null && buscar.equalsIgnoreCase(linea)){
                    resulado = "Pelicula " + linea + "Ubicada en el indice " + i;
                    break;
                }
                linea = bufferedReader.readLine();
                i++;
            }
            bufferedReader.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return resulado;
    }

    @Override
    public void crear(String nombreArchivo) throws AccesoDatosEx {
        File archivo = new File(nombreArchivo);
        try{
            PrintWriter writer = new PrintWriter(new FileWriter(archivo));
            writer.close();
            System.out.println("Se ha creado el archivo correctamente");
        }catch (IOException ex){

        }
    }

    @Override
    public void borrar(String nombreArchivo) throws AccesoDatosEx {
        File archivo = new File(nombreArchivo);
        archivo.delete();
        System.out.println("Se ha borrado el archivo correctamente");
    }
}
