package instanceOF;

public class Main {

    public static void main(String[] args) {
         FiguraGeometrica figuraGeometrica;
         figuraGeometrica = new Elipse();

         //Determina solo un tipo de que corresponda a la gerarquia
         System.out.println("Solo un tipo");
         determinaTipo(figuraGeometrica);

         System.out.println("\nTodos sus tipos");
         determinaTodosLosTipos(figuraGeometrica);
    }

    private static void determinaTodosLosTipos(FiguraGeometrica figuraGeometrica) {
        if(figuraGeometrica instanceof Elipse){
            System.out.println("Es un elipse");
        }

        if (figuraGeometrica instanceof Circulo) {
            System.out.println("Es un circulo");
        }

        if(figuraGeometrica instanceof  FiguraGeometrica){
            System.out.println("Es una figura geometrica");
        }

        if(figuraGeometrica instanceof Object){
            System.out.println("Es un object");
        }else{
            System.out.println("No se encontro el tipo");
        }
    }

    private static void determinaTipo(FiguraGeometrica figuraGeometrica) {
        if(figuraGeometrica instanceof Elipse){
            System.out.println("Es un elipse");
        }else if (figuraGeometrica instanceof Circulo) {
            System.out.println("Es un circulo");
        }else if(figuraGeometrica instanceof  FiguraGeometrica){
            System.out.println("Es una figura geometrica");
        }else if(figuraGeometrica instanceof Object){
            System.out.println("Es un object");
        }else{
            System.out.println("No se encontro el tipo");
        }
    }
}
