package forech;

import Model.Persona;

public class ForEach {

    public static void main(String[] args) {

        Persona persona [] =  {(new Persona("Juan")) , new Persona("Karla"), new Persona("Abraham")};

        for (Persona persona1: persona) {
            String nombre = persona1.getNombre();
            System.out.println("Son : "+nombre);
        }

        System.out.println(" ");

        int numeros []  = {99,43,78,12};

        for (int n: numeros) {
            System.out.println(n);
        }
    }
}
