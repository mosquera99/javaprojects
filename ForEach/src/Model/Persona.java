package Model;

public class Persona {

    private static int contadorPersona;
    private final int idPersona;
    private String nombre;

     public Persona(String  nombre){
        idPersona = ++contadorPersona;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
