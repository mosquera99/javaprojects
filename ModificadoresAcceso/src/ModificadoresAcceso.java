import Paquete2.*;
import paquete1.Clase2;

public class ModificadoresAcceso {

    public static void main(String[] args) {

        System.out.println("Ejecucion clase 2 mismmo package");
        new Clase2().pruebaClase2();

        System.out.println(" ");
        System.out.println("Ejecucion clase 3 otro package con extends");
        new Clase3().pruebaClase3();
        System.out.println(" ");
        System.out.println("Ejecucion clase 4 otro package sin extends");
        new Clase4().pruebaClase4();
    }
}
