package Paquete2;

import paquete1.Clase1;

public class Clase4 {

    public Clase4(){

    }

    public void pruebaClase4(){
        Clase1 clase1 = new Clase1();

        System.out.println("Clase 4 numPublic: "+clase1.numPublic);
        System.out.println("Clase 4 metodoPublic: "+clase1.metodoPublic());
    }
}
