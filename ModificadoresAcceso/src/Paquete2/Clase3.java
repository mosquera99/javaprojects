package Paquete2;

import paquete1.Clase1;

public class Clase3 extends Clase1{

    public Clase3(){
        super(1,2);
    }

    public void pruebaClase3(){
        Clase1 clase1 = new Clase1();
        System.out.println("Clase 3 numPublic: "+clase1.numPublic);

        System.out.println("Clase 3 metodoPublic: "+clase1.metodoPublic());
    }
}
