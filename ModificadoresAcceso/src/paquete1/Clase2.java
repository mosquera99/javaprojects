package paquete1;

public class Clase2  {

    public Clase2(){
        new Clase1();
        new Clase1(4);
        new Clase1(1,4);
        new Clase1(7,4,8);
    }

    public void pruebaClase2(){
        Clase1 clase1 = new Clase1();

        System.out.println("Public: "+clase1.numPublic);
        System.out.println("Protected: "+clase1.numProtected);
        System.out.println("Default: "+clase1.numDefault);

        System.out.println(" ");
        System.out.println("Metodo Public: "+clase1.metodoPublic());
        System.out.println("Metodo Protected: "+clase1.metodoProtected());
        System.out.println("Metodo Default: "+clase1.metodoDefault());

    }
}
