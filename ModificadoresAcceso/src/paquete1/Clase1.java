package paquete1;

public class Clase1 {

    public int numPublic = 9;
    protected int numProtected = 7;
    int numDefault = 3;
    private int numPrivate = 45;

    public  Clase1(){
    }

    public  Clase1(int e){
        System.out.println("Constructor public");
    }

    protected Clase1(int i,int o){
        System.out.println("Constructor protected");
    }

    Clase1 (int j,int k,int p){
        System.out.println("Constructor default");
    }

    private Clase1(int l,int t,int a,int y){
        System.out.println("Constructor default");
    }

    public int metodoPublic(){
        return 8;
    }
    protected int metodoProtected(){
        return 3;
    }
     int metodoDefault(){
        return 7;
    }
    private int metodoPrivate(){
        return 10;
    }



}
