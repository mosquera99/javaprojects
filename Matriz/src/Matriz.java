public class Matriz {

    public static void main(String[] args) {
        int numeros [][];
        numeros = new int [2][2];
        numeros[0][0] = 5;
        numeros[0][1] = 55;
        numeros[1][0] = 67;
        numeros[1][1] = 90;

        System.out.println("Valor matriz 0-0: "+numeros[0][0]);
        System.out.println("Valor matriz 0-1: "+numeros[0][1]);
        System.out.println("Valor matriz 1-0: "+numeros[1][0]);
        System.out.println("Valor matriz 1-1: "+numeros[1][1]);

        Persona personas [][];
        personas = new Persona[2][2];
        personas[0][0] = new Persona("Abraham");
        personas[0][1] = new Persona("Luis");
        personas[1][0] = new Persona("Savier");
        personas[1][1] = new Persona("Adrian");

        for (int i=0; i<personas.length;i++){
            for (int j=0; j<personas[i].length;j++){
                System.out.println("Matriz en el indice :"+i+"-"+j+" "+"Valor matriz: "+personas[i][j]);
            }
        }
    }
}
