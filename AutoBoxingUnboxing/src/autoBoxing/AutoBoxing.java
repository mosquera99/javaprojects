package autoBoxing;

public class AutoBoxing {
    public static void main(String[] args) {

        Integer enteroObj = 16;
        Double decimalObj = 40.5;
        Float flotanteObj = 22.5F;
        System.out.println("flotanteObj = " + flotanteObj.floatValue());
        System.out.println("decimalObj = " + decimalObj.doubleValue());
        System.out.println("enteroObj = " + enteroObj.intValue());

        System.out.println(" ");

        int entero = enteroObj;
        double decimal = decimalObj;
        float flotante = flotanteObj;

        System.out.println("flotante = " + flotante);
        System.out.println("decimal = " + decimal);
        System.out.println("entero = " + entero);
    }
}
